---
title: "Revised Plots"
author: "Seba De Bona"
date: ""
output: 
  html_document:
    toc: true
    toc_float: true
    toc_depth: 4
    theme: flatly
---

## Preliminary steps

Loading the package, and necessary dependencies

```{r, message = F, warning = F}
library(DDDEvo)
library(plyr)
library(tidyr)
library(lme4)
library(MASS)
```

Loading the dataset and subsetting single-sex sub-datasets:

```{r}
data = DDDdata
fata <- subset(data, sex == 'F')
mata <- subset(data, sex == 'M')
```

Storing original plotting parameters to restore them later

```{r}
op <- par()
```

## Fitting Models

Here the models later used to build plots are run and stored, together with functions to calculate the slope of density dependence for those models.

### Natal dispersal probability
#### Females

```{r, results = 'asis'}
model.pnat <- readRDS(file.path(here::here(), "vignettes","natal_dispersal_females.rds"))
knitr::kable(summary(model.pnat)$coefficients)

slopePnat <- SlopeNatal(model=model.pnat, dataset=subset(fata, habitat.birth=='P' & mummoved==0), formula.slo=~s_land_density.mid*s_time.birth, formula.int=~s_land_density.mid*s_time.birth) #MIND THE ORDER HERE! Look at the model, and check how the components of intercept and slope are made. 
```


#### Males

```{r, results = 'asis'}
model.pnatm <- readRDS(file.path(here::here(), "vignettes", "natal_dispersal_males.rds"))
knitr::kable(summary(model.pnatm)$coefficients)

slopePnatm<-SlopeNatal(model=model.pnatm, dataset=subset(mata, habitat.birth=='P' & mummoved==0), formula.slo=~s_land_density.mid, formula.int=~s_land_density.mid) #MIND THE ORDER HERE! Look at the model, and check how the components of intercept and slope are made. 
```


### Adult dispersal probability
#### Females

```{r, results = 'asis'}
model.pmov <- readRDS(file.path(here::here(), "vignettes", "adult_dispersal_females.rds"))
knitr::kable(summary(model.pmov)$coefficients)

slopePmov <- Slope(model=model.pmov, data=subset(fata, habitat=='P'), formula.slo = ~s_time+s_dkin, formula.int = ~ s_time+s_dkin) #MIND THE ORDER HERE! Look at the model, and check how the components of intercept and slope are made. Here, time comes before landscape density, because of the model formulation.
# NOTE: the values of kin are set to 0 in the Slope function.
```


#### Males

```{r, results = 'asis'}
model.pmovm <- readRDS(file.path(here::here(), "vignettes", "adult_dispersal_males.rds"))
knitr::kable(summary(model.pmovm)$coefficients)

slopePmovm <- Slope(model=model.pmovm, data=subset(mata, habitat=='P'), formula.slo = ~ s_time + s_dkin, formula.int = ~ s_time + s_dkin)
# ONCE AGAIN, kin set to 0 in the function Slope
```


## Plots

### Figure 1

Figure 1 corresponds to a panel representing landscape density throughout the introduction and local density (average, maximum, minimum) in occupied pools.

```{r, warning = F, fig.height = 8}
# calculating necessary values form dataset
densities <- ddply(subset(data, habitat='P'), .(sampling, land_density, loc_density), nrow)
densities <- ddply(densities, .(sampling, land_density), summarise, low=min(loc_density, na.rm=T), upp=max(loc_density, na.rm=T), avg=mean(loc_density, na.rm=T), low.q=quantile(loc_density, 0.025, na.rm=T), up.q=quantile(loc_density, 0.975, na.rm=T))
par(op)

Figure1 <- function(..., leg.inset, leg.cex, leg.labels,
                    cex_axis = 0.6, cex_lab = 0.6){
  plot(densities$land_density~I(as.numeric(densities$sampling)-1), type='l', ylab='', ylim=c(0,11),lwd=1.2, xlab='', cex.axis=cex_axis, yaxt = 'n', bty = 'n')
  axis(2, at = c(0,2,4,6,8,10), labels = c(0,2,4,6,8,10), las = 2, cex.axis=cex_axis)
  title(ylab='population density (ind/m)', line=2, cex.lab=cex_lab)
  legend("topright", legend=leg.labels[1], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
  
  plot(I(log10(densities$avg))~I(as.numeric(densities$sampling)-1), type='l', ylab='', xlab='', ylim=c(-1.3,2),lwd=1.2, yaxt='n', cex.axis=cex_axis,...)
  axis(2, at=c(-1,0,1,2), labels=c(0.1,1,10, 100), cex.axis=cex_axis, las=1)
  axis(2, at=seq(-1,2, by=0.1), lwd=0.5, labels=F, tck=-0.02)
  title(ylab='density in occupied pools (ind/m)', line=2, cex.lab=cex_lab)
  title(xlab='time (months)', line=0, outer=T, cex.lab=cex_lab)
  lines(I(log10(densities$upp))~I(as.numeric(densities$sampling)-1), lty=3, lwd=0.8)
  lines(I(log10(densities$low))~I(as.numeric(densities$sampling)-1), lty=3, lwd=0.8)
  lines(I(log10(densities$low.q))~I(as.numeric(densities$sampling)-1), lty=5, lwd=0.8)
  lines(I(log10(densities$up.q))~I(as.numeric(densities$sampling)-1), lty=5, lwd=0.8)
  legend("topright", legend=leg.labels[2], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
}

par(mfrow=c(2,1), oma=c(1.5,1,1,0), mar=c(3,4,1.5,2))
Figure1(bty='n', leg.inset=c(0,0), leg.cex=1.2, leg.labels = c('(a)','(b)'))
```


### Figure 2

Two-panel figure depicting average natal and adult dispersal propensity over time.

```{r, fig.height = 7}
### *** Panel 2.1: natal dispersal propensity through time (estimated from model and mean+/-error from data)
Panel2.1 <- function(..., cex_points = 0.7){
  # plotting avg probability of moving over time (and real data in grey)
  plot(0, col='white', xlim = c(0,60), ylim = c(0,1), xlab = 'time (months)', las = 1, ylab = '',...)
  
  moplot <- ddply(subset(mata, habitat == 'P' & mummoved == 0), .(sampling.birth, time), summarise, p=mean(p_natal), ns=sum(p_natal))
  moplot$n <- ddply(subset(mata, habitat == 'P' & mummoved == 0), .(sampling.birth, time), nrow)[,'V1']
  moplot$nf <- moplot$n-moplot$ns
  moplot$upper <- bin.error(t=moplot, upper=T)
  moplot$lower <- bin.error(t=moplot, upper=F)
  points(p~I(as.numeric(sampling.birth)+0.25), data=moplot, pch=16, col=rgb(0.4,0.4,0.4,1), cex=cex_points)
  segments(x0=as.numeric(moplot$sampling.birth)+0.25, y0=moplot$upper, y1=moplot$lower, col=rgb(0.4,0.4,0.4,1))
  lines(p~I(as.numeric(sampling.birth)+0.25), data=moplot, lty=1, col=rgb(0.4,0.4,0.4,1), lwd=0.5)
  
  toplot <- ddply(subset(fata, habitat == 'P' & mummoved == 0), .(sampling.birth, time), summarise, p=mean(p_natal), ns=sum(p_natal))
  toplot$n <- ddply(subset(fata, habitat == 'P' & mummoved == 0), .(sampling.birth, time), nrow)[,'V1']
  toplot$nf <- toplot$n-toplot$ns
  toplot$upper <- bin.error(t=toplot, upper=T)
  toplot$lower <- bin.error(t=toplot, upper=F)
  points(p~I(as.numeric(sampling.birth)-0.25), data=toplot, pch=1, col=rgb(0.7,0.7,0.7,1), cex=cex_points)
  segments(x0=as.numeric(toplot$sampling.birth)-0.25, y0=toplot$upper, y1=toplot$lower, col=rgb(0.7,0.7,0.7,1))
  lines(p~I(as.numeric(toplot$sampling.birth)-0.25), data=toplot, lty=1, col=rgb(0.7,0.7,0.7,1), lwd=0.5)
}

### *** Panel 2.2: adult dispersal propensity through time (estimated from model and mean +/- error from data)
Panel2.2 <- function(..., cex_points=0.7){
  plot(0, col='white', xlim = c(0,61), ylim = c(0,1), xlab = 'time (months)', las = 1, ylab = '', ...)
  
  moplot <- ddply(subset(mata, habitat == 'P' & !is.na(p_moving)), .(sampling, time), summarise, p=mean(p_moving, na.rm = T), ns=sum(p_moving, na.rm = T))
  moplot$n <- ddply(subset(mata, habitat == 'P' & !is.na(p_moving)), .(sampling, time), nrow)[,'V1']
  moplot$nf <- moplot$n-moplot$ns
  # moplot$p_hat <- (moplot$ns+(1.96^2)/2)/(moplot$n+1.96^2)
  # p_hat is an altiernative to p as the mean of outcomes, as prescribed by Wilson's score interval
  moplot$upper <- bin.error(t=moplot, upper=T)
  moplot$lower <- bin.error(t=moplot, upper=F)
  points(p~I(as.numeric(sampling)+0.25), data=moplot, pch=16, col=rgb(0.4,0.4,0.4,1), cex=cex_points)
  lines(p~I(as.numeric(sampling)+0.25), data=moplot, lty=1, col=rgb(0.4,0.4,0.4,1), lwd=0.5)
  segments(x0=moplot$time+1+0.25, y0=moplot$upper, y1=moplot$lower, col=rgb(0.4,0.4,0.4,1))

  toplot <- ddply(subset(fata, habitat == 'P' & !is.na(p_moving)), .(sampling, time), summarise, p=mean(p_moving, na.rm = T), ns=sum(p_moving, na.rm = T))
  toplot$n <- ddply(subset(fata, habitat == 'P' & !is.na(p_moving)), .(sampling, time), nrow)[,'V1']
  toplot$nf <- toplot$n-toplot$ns
  toplot$upper <- bin.error(t=toplot, upper=T)
  toplot$lower <- bin.error(t=toplot, upper=F)
  points(p~I(as.numeric(sampling)-0.25), data=toplot, pch=1, col=rgb(0.7,0.7,0.7,1), cex=cex_points)
  lines(p~I(as.numeric(sampling)-0.25), data=toplot, lty=1, col=rgb(0.7,0.7,0.7,1), lwd=0.5)
  segments(x0=toplot$time+1-0.25, y0=toplot$upper, y1=toplot$lower, col=rgb(0.7,0.7,0.7,1))
  
  # adding significance graph
  segments(x0 = max(toplot$time) + 3,
           y1 = toplot$p[which.max(toplot$time)],
           y0 = moplot$p[which.max(moplot$time)])
  text(x = max(toplot$time) + 4.5,
       y = mean(c(toplot$p[which.max(toplot$time)], moplot$p[which.max(moplot$time)])),
       labels = '***',
       srt = 90)
}


# defining function to draw complete figure
Figure2 <- function(..., leg.inset, leg.cex, leg.labels){
  Panel2.1(...)
  mtext('Natal dispersal probability', side=2, line=2.5, cex=0.9)
  legend("topright", legend=leg.labels[1], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
  par(xpd = T) # allows legend to be printed outside margins
  legend(x=20, y = 1.1, legend = c('F', 'M'), 
         pch = c(1,19), col = c(rgb(0.7,0.7,0.7,1), rgb(0.4,0.4,0.4,1)),
         bty = 'n',
         cex = 0.8,
         horiz = T)
  par(xpd = F) # resets default
  
  Panel2.2(...)
  mtext('Adult dispersal probability', side=2, line=2.5, cex=0.9)
  legend("topright", legend=leg.labels[2], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
  mtext('time (months)', side=1, line=2.5, cex=0.9)
  
}

par(mfrow=c(2,1), oma=c(2,1.5,1,1), mar=c(3,4,1,1))
Figure2(bty='n', cex.axis=0.8, cex.lab=0.8, leg.inset=c(0,0), leg.cex=1, leg.labels = c('(a)','(b)'))
```


### Figure 3 - Natal Dispersal

Two by two panel representing natal dispersal propensity in females (a,b) and males (c,d) over values of landscape density (a,c) and time (b,d). 

```{r, fig.height = 7}
### *** Panel 3a: strenght of density dependence on natal dispersal against landscape density in females
Panel3a <- function(..., cex_points = 0.5){
  plot(0, col='white', xaxt='n', 
       xlim = (c(0,12) - attr(DDDdata$s_land_density, 'scaled:center'))/attr(DDDdata$s_land_density, 'scaled:scale'), cex.axis=0.7,...)
  abline(h=0, lty=3)
  
  PlotPINat(model=model.pnat, formula=~s_land_density.mid*s_time.birth, slope=slopePnat, x='density', type='bars', col = rgb(0.7, 0.7, 0.7, 1))
  points(slopePnat$slopeDD~slopePnat$s_land_density.mid, col=rgb(0.7,0.7,0.7,1), pch=19, cex = cex_points)
  
  axis(1, at=(c(0,2,4,6,8,10,12) - attr(DDDdata$s_land_density, 'scaled:center'))/attr(DDDdata$s_land_density, 'scaled:scale'), labels = c(0,2,4,6,8,10,12), cex.axis = 0.7)
}


### *** Panel 3c: strenght of density dependence on natal dispersal against landscape density in males

Panel3c <- function(...,cex_points=0.5){
  plot(0, col='white', xaxt='n', 
       xlim = (c(0,12) - attr(DDDdata$s_land_density, 'scaled:center'))/attr(DDDdata$s_land_density, 'scaled:scale'),cex.axis=0.7, ...)
  abline(h=0, lty=3)
  
  PlotPINat(model=model.pnatm, formula=~s_land_density.mid, slope=slopePnatm, x='density', type='bars', col = rgb(0.4, 0.4, 0.4, 1))
  points(slopePnatm$slopeDD~slopePnatm$s_land_density.mid, col=rgb(0.4, 0.4, 0.4, 1), pch=19, cex = cex_points)
  
  axis(1, at=(c(0,2,4,6,8,10,12) - attr(DDDdata$s_land_density, 'scaled:center'))/attr(DDDdata$s_land_density, 'scaled:scale'), labels = c(0,2,4,6,8,10,12), cex.axis = 0.7)
}


### *** Panel 3b-d: strenght of density dependence on natal dispersal propensity through time, with fixed landscape density at an average value after the population peak
# calculating the average landscape density

densities <- ddply(subset(DDDdata, habitat='P'), .(sampling, land_density ,s_land_density), nrow)
avg_equilibrium_land_density <- mean(subset(densities, as.numeric(sampling)>0)$s_land_density)
# its actual value
avg_equilibrium_land_density*attr(DDDdata$s_land_density, 'scaled:scale') + attr(DDDdata$s_land_density, 'scaled:center')

slopePnat_Man <- SlopeNatal_Man(model=model.pnat, dataset=subset(fata, habitat=='P' & mummoved == 0), formula.slo=~s_land_density.mid*s_time.birth, formula.int=~s_land_density.mid*s_time.birth, manual_density = avg_equilibrium_land_density)

slopePnat_Male <- SlopeNatal_Man(model=model.pnatm, dataset=subset(mata, habitat=='P' & mummoved == 0), formula.slo=~s_land_density.mid, formula.int=~s_land_density.mid, manual_density = avg_equilibrium_land_density)

Panel3b <- function(...){
  plot(0, col='white',xlim = c(0,60), cex.axis = 0.7, ...)
  abline(h=0, lty=3)
  
  PlotPINat(model=model.pnat, formula=~s_land_density.mid*s_time.birth, slope=slopePnat_Man, x='time', type='shade', color = rgb(0.5, 0.5, 0.5, 0.2))
  lines(slopePnat_Man$slopeDD~I(as.numeric(slopePnat_Man$sampling.birth)), lty=1)
}

Panel3d <- function(...){
  plot(0, col='white',xlim = c(0,60), cex.axis = 0.7,...)
  abline(h=0, lty=3)
  
  PlotPINat(model=model.pnatm, formula=~s_land_density.mid, slope=slopePnat_Male, x='time', type='shade', color = rgb(0.5, 0.5, 0.5, 0.7))
  lines(slopePnat_Male$slopeDD~I(as.numeric(slopePnat_Male$sampling.birth)), lty=1)
}

# wrapping all panels in one function to produce figure
Figure3 <- function(..., leg.inset, leg.cex, leg.labels, cex_text = 0.6, cex_main = 0.8){
  Panel3a(...)
  mtext('landscape density (ind/m)', side=1, line=2, cex=cex_text)
  legend("topright", legend=leg.labels[1], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
  mtext('females', side=3, line=-2, cex = 0.6)
  
  Panel3b(...)
  mtext('time (months)', side=1, line=2, cex=cex_text)
  legend("topright", legend=leg.labels[2], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
  mtext('females', side=3, line=-2, cex = 0.6)

  Panel3c(...)
  mtext('landscape density (ind/m)', side=1, line=2, cex=cex_text)
  legend("topright", legend=leg.labels[3], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
  mtext('males', side=3, line=-2, cex = 0.6)

  Panel3d(...)
  mtext('time (months)', side=1, line=2, cex=cex_text)
  legend("topright", legend=leg.labels[4], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
  text('MALES', x = -20, y = 2.3, cex = 0.8, outer = T)
  mtext('males', side=3, line=-2, cex = 0.6)
  
  mtext('slope of local density dependence', side=2, line=0.5, cex=cex_text, outer = T)
}


par(mfrow=c(2,2), oma=c(2,5,1,1), mar=c(4,2,1,1) )
Figure3(ylim = c(-1,2.3), bty='n', xlab='', ylab='', leg.inset=c(0, 0), leg.cex=1.2, leg.labels = c('(a)','(b)','(c)','(d)'))

```


### Figure 4 - Adult Dispersal

Two by two panel representing adult dispersal propensity in females (a,b) and males (c,d) over values of landscape density (a,c) and time (b,d). 

```{r, fig.height = 7}
### *** Panel 4a: strength of density dependence on adult dispersal propensity across landscape density. The arrows indicate the cronological order for females
Panel4a <- function(..., cex_points = 0.5){
  plot(0, col='white',  xaxt='n', 
       xlim = (c(0,12) - attr(DDDdata$s_land_density, 'scaled:center'))/attr(DDDdata$s_land_density, 'scaled:scale'), cex.axis=0.7, ...)
  abline(h=0, lty=3)
  axis(1, at=(c(0,2,4,6,8,10) - attr(DDDdata$s_land_density, 'scaled:center'))/attr(DDDdata$s_land_density, 'scaled:scale'), labels = c(0,2,4,6,8,10),cex.axis=0.7)
  
  PlotPI(model=model.pmov, formula=~s_time + s_dkin, slope=slopePmov, x='density', type='bars', col = rgb(0.7, 0.7, 0.7, 1))
  points(slopePmov$slopeDD~slopePmov$s_land_density, pch=19, col=rgb(0.7, 0.7, 0.7, 1), cex = cex_points)
}

### *** Panel 4c: same as Panel 4a, but for males

Panel4c <- function(..., cex_points = 0.5){
  plot(0, col='white', xaxt='n', xlim = (c(0,12) - attr(DDDdata$s_land_density, 'scaled:center'))/attr(DDDdata$s_land_density, 'scaled:scale'), cex.axis=0.7, ...)
  abline(h=0, lty=3)
  axis(1, at=(c(0,2,4,6,8,10) - attr(DDDdata$s_land_density, 'scaled:center'))/attr(DDDdata$s_land_density, 'scaled:scale'), labels = c(0,2,4,6,8,10), cex.axis=0.7)
  
  PlotPI(model=model.pmovm, formula=~s_time + s_dkin, slope=slopePmovm, x='density', type='bars', col = rgb(0.4, 0.4, 0.4, 1))
  points(slopePmovm$slopeDD~slopePmovm$s_land_density, pch=19, col=rgb(0.4, 0.4, 0.4, 1), cex = cex_points)
}

### *** Panel 4b: strength of density dependence on adult dispersal propensity through time. the shaded area corresponds to the 95% prediction interval, the grey dots are the point estimates for random slope. For females...
Panel4b <- function(...){
  plot(0, col='white', xlim = c(0,60), cex.axis = 0.7, ...)
  
  abline(h=0, lty=3)
  
  
  PlotPI(model=model.pmov, formula=~s_time + s_dkin, slope=slopePmov, color = rgb(0.5, 0.5, 0.5, 0.2))
  lines(slopePmov$slopeDD~I(as.numeric(slopePmov$sampling)), lty=1)
}

### *** Panel 4d: ... and again for males
Panel4d <- function(...){
  plot(0, col='white', xlim = c(0,60), cex.axis = 0.7, ...)
  
  abline(h=0, lty=3)
  
  PlotPI(model=model.pmovm, formula=~s_time + s_dkin, slope=slopePmovm, color = rgb(0.5, 0.5, 0.5, 0.7))
  lines(slopePmovm$slopeDD~I(as.numeric(slopePmovm$sampling)), lty=1)
}



# wrapping all panels in one function to produce figure
Figure4 <- function(..., leg.inset, leg.cex, leg.labels, cex_text = 0.6, cex_main = 0.8){
  Panel4a(...)
  mtext('landscape density (ind/m)', side=1, line=2, cex=cex_text)
  legend("topright", legend=leg.labels[1], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
  mtext('females', side=3, line=-2, cex = 0.6)
  
  Panel4b(...)
  mtext('time (months)', side=1, line=2, cex=cex_text)
  legend("topright", legend=leg.labels[2], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
  mtext('females', side=3, line=-2, cex = 0.6)

  Panel4c(...)
  mtext('landscape density (ind/m)', side=1, line=2, cex=cex_text)
  legend("topright", legend=leg.labels[3], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
  mtext('males', side=3, line=-2, cex = 0.6)

  Panel4d(...)
  mtext('time (months)', side=1, line=2, cex=cex_text)
  legend("topright", legend=leg.labels[4], inset=leg.inset, cex=leg.cex, xpd=TRUE, bty='n')
  mtext('males', side=3, line=-2, cex = 0.6)
  
  mtext('slope of local density dependence', side=2, line=0.5, cex=cex_text, outer = T)
}


par(mfrow=c(2,2), oma=c(2,5,1,1), mar=c(4,2,1,1) )
Figure4(ylim = c(-1,2.3), bty='n', xlab='', ylab='', leg.inset=c(0, 0), leg.cex=1.2, leg.labels = c('(a)','(b)','(c)','(d)'))
```
