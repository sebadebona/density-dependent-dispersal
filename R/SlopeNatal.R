#' Get slope of density dependence (for natal)
#'
#' Calculates the estimated slope of density-dependence given a fitted model where local density features as one of the covariates. Note: use this function for natal; for adult use \code{\link{Slope}}.
#'
#' @param model Fitted GLMM, where local density appears as one of the covariates, possibly interaction with other variables.
#' @param dataset Data fed into model. Used to calculate covariate values (time and landscape density).
#' @param formula.slo Formula identifying the slope of density dependence (i.e. defining which other covariates *interact* with local density in determining the dispersal trait). Note: the order needs to reflect the order of appearance of these interactions in the model.
#' @param formula.int Formula identifying the variables that contributes to the intercept of density dependence (i.e. variables that *do not interact* with local density).
#' @param manual_density Allow to set a fixed value for landscape density, if using \code{SlopeNatal_Man}.
#'
#' @export

SlopeNatal <- function(model, dataset, formula.slo, formula.int){
  toplot <- as.data.frame(unique(cbind(dataset$sampling.birth, dataset$s_time.birth, dataset$s_land_density.mid)))
  colnames(toplot)<-c('sampling.birth','s_time.birth','s_land_density.mid')
  toplot <- toplot[order(toplot$sampling.birth),]
  mm.slo <- model.matrix(as.formula(formula.slo), toplot)
  mm.int <- model.matrix(as.formula(formula.int), toplot)
  isolate <- grep('s_loc_density', names(fixef(model)))
  # extracting model estimates belonging to slope and storing them in a vector
  fixed.slo <- fixef(model)[isolate]
  fixed.int <- fixef(model)[-isolate]
  toplot$slopeDD <- mm.slo%*%fixed.slo
  toplot$intDD <- mm.int%*%fixed.int
  # I can also extract the estimates for individual random intercept and slope
  #match(as.numeric(colnames(ranef(model)$sampling.mid))
  randomEff <- data.frame(sampling.birth=rownames(ranef(model)$sampling.birth),
                          ranslope=ranef(model)$sampling.birth[,'s_loc_density.mid'],
                          ranint=ranef(model)$sampling.birth[,1])
  toplot <- merge(toplot, randomEff, by='sampling.birth')
  return(toplot)
}

### * Creating a version of the same function to calculate the the slope but fixing the value of landscape density

#' SlopeNatal_Man
#'
#' @export

SlopeNatal_Man <- function(model, dataset, formula.slo, formula.int, manual_density){
  toplot <- as.data.frame(unique(cbind(dataset$sampling.birth, dataset$s_time.birth, dataset$s_land_density.mid)))
  colnames(toplot)<-c('sampling.birth','s_time.birth','s_land_density.mid')
  toplot <- toplot[order(toplot$sampling.birth),]
  toplot$s_land_density.mid <- rep(manual_density, nrow(toplot))
  mm.slo <- model.matrix(as.formula(formula.slo), toplot)
  mm.int <- model.matrix(as.formula(formula.int), toplot)
  isolate <- grep('s_loc_density', names(fixef(model)))
  # extracting model estimates belonging to slope and storing them in a vector
  fixed.slo <- fixef(model)[isolate]
  fixed.int <- fixef(model)[-isolate]
  toplot$slopeDD <- mm.slo%*%fixed.slo
  toplot$intDD <- mm.int%*%fixed.int
  # I can also extract the estimates for individual random intercept and slope
  #match(as.numeric(colnames(ranef(model)$sampling.mid))
  randomEff <- data.frame(sampling.birth=rownames(ranef(model)$sampling.birth),
                          ranslope=ranef(model)$sampling.birth[,'s_loc_density.mid'],
                          ranint=ranef(model)$sampling.birth[,1])
  toplot <- merge(toplot, randomEff, by='sampling.birth')
  return(toplot)
}
