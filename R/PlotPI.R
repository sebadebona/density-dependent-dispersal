#' Plot Prediction Intervals (adult dispersal)
#' 
#' This function generates simulations for the slope of density dependence, derived from the estimates and error produced by a generalised linear model, to estimate prediction intervals. Optionally plots the PI on a previously drawn plot. Note: use this function for adult dispersal; for natal dispersal use \code{\link{PlotPINat}}.
#'
#'@param slope Output object of the function \code{\link{Slope}}
#'@param model Fitted generalised linear model, where a dispersal trait is the response variable, and local density appears as an explanatory variable.
#'@param formula The formula identifying the slope of density dependence (i.e. defining which other covariates *interact* with local density indetermining the dispersal trait). Note: the order needs to reflect the order of appearance of these interactions in the model
#'@param nsims Number of simulations to be drawn to produce PI
#'@param x Variable at the x-axis in the plot. Can take up two values: `time` or `density`.
#'@param type Defines the way prediction intervals are drawn. `shade` draws a semi-transparent polygon that tracks the PI breadth along the values of x (preferable if plotting time on the x-axis); `bars` draws segments orthogonal to the x-axis (preferable when plotting density on the x-axis).
#'@param suppressPlots Logical value. Specifies whether a the PI values are to be plotted (`TRUE`) or just need to be estimated for other uses (`FALSE`).
#'@param color Defines the color of the shaded area or bars to be plotted. Preferably in rbg values, defining its transparency too.
#'@param epsilon Optional. Small value used to offset the bar (or segment) from the actual x value. Useful when plotting multiple PI, in plots depicting, for example, females and males in different colors.
#' @param set.kin Set value of kinship, to marginalize over it in the calculation of slope.
#'
#' @export

PlotPI <- function(slope, model, formula, nsims=100000, x='time', type='shade', suppressPlots=F, color = rgb(0.5, 0.5, 0.5,0.5), epsilon = 0, set.kin = 0){
  
  topredict<-data.frame(s_land_density=slope$s_land_density, 
                        s_time=slope$s_time, 
                        s_dkin = rep(set.kin, nrow(slope)))
  # I can create a model matrix with all the covariates which interacts with local density. Since my aim is to simulate values for the slope of loc density dependence, I need to pick: (1) the estimate for the effect of loc density alone; (2) and the estimates for the interactions with other covariates (e.g. land density, time, both(if 3-way interactions)), which have to be multiplied with the values of the relative covariate (e.g., time*(Estimate for time:loc_density interaction)). This basically corresponds to multiplying a model matrix (with only covariates that interact with s_loc_density) by all the Estimates that have to do with local density. So if I create a model matrix with this covariates that interact with loc_density, I'll have an intercept, and then values of these covariates. I can then use matrix multiplication, by "shifting" the meaning of the model matrix column. For instance, my intercept won't be the True intercept, by what assures the estimate for the effect of Loc_density alone to always be included.
  mm <- model.matrix(as.formula(formula), topredict)
  # the following code isolates only the estimates which are related to loc_density
  isolate <- grep('s_loc_density', names(fixef(model)))
  # extracting only fixed effects of interest
  fixed <- mvrnorm(nsims, mu=fixef(model)[isolate], Sigma=vcov(model)[isolate, isolate])
  # creating prediction matrix, nsims*60
  predictions<-t(mm%*%t(fixed))
  # obtaining 95% lower and upper prediction intervals
  PI <- apply(predictions, 2, function(x){quantile(x, c(0.025,0.975))})
  # renaming columns of the prediction quantiles, so that they correspond to the sampling month they relate to. The transformation brings back the unscaled time, and the +1 converts the time into sampling
  colnames(PI) <- round(slope$s_time*attr(DDDdata$s_time, 'scaled:scale'))+1
  # creating a cbind with X and Y to be plotted for polygon
  coo <- data.frame(time=as.numeric(colnames(PI)),lower=PI[1,],upper=PI[2,],density=slope$s_land_density)
  if(type=='shade'){
    if(x=='time'){
      # drawing the polygon
      if(!suppressPlots){
        polygon(x=c(coo$time,rev(coo$time))+epsilon, y=c(coo$lower, rev(coo$upper)), col=color, border=NA )
      }
    } else if(x=='density') {
      coo <- coo[order(coo$density),]  
      # drawing the polygon
      if(!suppressPlots){
        polygon(x=c(coo$density,rev(coo$density))+epsilon, y=c(coo$lower, rev(coo$upper)), col=color, border=NA ) 
      }
    }
  } else if(type=='bars'){
    if(x=='time'){
      # drawing bars
      if(!suppressPlots){
        segments(x0=coo$time+epsilon, y0=coo$lower, y1=coo$upper, col=color )
      }
    } else if(x=='density'){
      coo <- coo[order(coo$density),]
      # drawing bars
      if(!suppressPlots){
        segments(x0=coo$density+epsilon, y0=coo$lower, y1=coo$upper, col=color )
      }
    }
  }
  return(coo)
  
}
