#' Remove NAs from selected columns
#' 
#' Given a dataframe, removes nas from selected column based on response variable to be used and whether the data has to be used for natal dispersal or adult dispersal analyses (this affects the variables selected)
#' 
#' @param myData A data frame to remove NAs from
#' @param response The response variable used in the model or plotting
#' @param noa \code{natal} or \code{adult} dispersal. Thi will affect the variables for which NAs are removed.
#' 
#' @export


cleanse <- function(myData, 
                    response, 
                    noa){
  
  if(noa == 'adult'){
   
    fixEff = c('loc_density', 'glob_density', 'time')
    ranEff = c('sampling', 'reach', 'FishID')
                   
  } else if(noa == 'natal'){
    
    fixEff = c('loc_density.mid', 'glob_density.mid', 's_time.birth')
    ranEff = c('sampling.mid', 'reach.birth', 'Maternity')
    
  }
  
  myData <- myData[complete.cases(myData[,c(response, fixEff, ranEff)]),]
  for(i in 1:length(ranEff)){
    myData[,ranEff[i]]<-as.factor(myData[,ranEff[i]])
  }
  return(myData)
}
