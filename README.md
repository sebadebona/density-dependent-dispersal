[![pipeline status](https://gitlab.com/sebadebona/density-dependent-dispersal/badges/master/pipeline.svg)](https://gitlab.com/sebadebona/density-dependent-dispersal/commits/master)

# Description

This repo reproduces the analyses and plots published in the paper "Spatio-temporal dynamics of density-dependent dispersal during a population colonisation", by De Bona et al. The paper studies the interactions between local and landscape density in determining natal and adult density-dependent dispersal, using a natural population of guppies as a study system.

The output of the analysis, including the script to generate them, can be found here:

- [Link](https://sebadebona.gitlab.io/density-dependent-dispersal)

# How to reproduce the analyses

You can clone this project locally by opening a terminal and typing:

```
git clone git@gitlab.com:sebadebona/density-dependent-dispersal.git
```

## How to prepare your R for the analyses

A preparation step is required before the analyses, and it entails installing a list of required packages. Copy-paste the following code in R or RStudio and execute:

```
install.packages(c("lme4", "sticky", "here", "tidyverse", "roxygen2", "car", "stargazer", "corrgram", "rmarkdown", "knitr", "RColorBrewer", "hexbin", "MASS", "broom.mixed", "kableExtra"))
```

## Data

The data is provided as a csv file (`data-raw/data.csv`). The script `prep-DDDdata.Rmd` is used to transform the data into an Rda file stored as `data/DDDdata.rda`.


## How to reproduce these analyses

The analyses are split in two vignettes, found in the vignette folder. The first vignette (`DDD-010-Results.Rmd`) reproduces model selection and displays and store the results; the second vignette (`DDD-020-Plots.Rmd`) loads the stored results and produces the plots presented as figures in the published paper.

These vignettes can be rendered locally to produce the same output as [here](https://sebadebona.gitlab.io/density-dependent-dispersal). To do this, access in the Temrinal the repo folder, then type `make install pkgdown`. This will generate an `index.html` file in the `docs/` folder, displaying the results of the analyses.

# References

For more information, see published paper at Link.
